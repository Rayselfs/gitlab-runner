## 執行步驟
1. 查看 K8s 默認角色 `edit` 的權限
    ```sh
    $ kubectl describe clusterrole edit
    # 主要針對 namespace 下的物件操作
    ```
    <details>
    <summary>權限列表</summary>
    <p>

    ```sh
    Name:         edit
    Labels:       kubernetes.io/bootstrapping=rbac-defaults
                rbac.authorization.k8s.io/aggregate-to-admin=true
    Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
    PolicyRule:
    Resources                                    Non-Resource URLs  Resource Names  Verbs
    ---------                                    -----------------  --------------  -----
    configmaps                                   []                 []              [create delete deletecollection patch update get list watch]
    endpoints                                    []                 []              [create delete deletecollection patch update get list watch]
    persistentvolumeclaims                       []                 []              [create delete deletecollection patch update get list watch]
    pods                                         []                 []              [create delete deletecollection patch update get list watch]
    replicationcontrollers/scale                 []                 []              [create delete deletecollection patch update get list watch]
    replicationcontrollers                       []                 []              [create delete deletecollection patch update get list watch]
    services                                     []                 []              [create delete deletecollection patch update get list watch]
    daemonsets.apps                              []                 []              [create delete deletecollection patch update get list watch]
    deployments.apps/scale                       []                 []              [create delete deletecollection patch update get list watch]
    deployments.apps                             []                 []              [create delete deletecollection patch update get list watch]
    replicasets.apps/scale                       []                 []              [create delete deletecollection patch update get list watch]
    replicasets.apps                             []                 []              [create delete deletecollection patch update get list watch]
    statefulsets.apps/scale                      []                 []              [create delete deletecollection patch update get list watch]
    statefulsets.apps                            []                 []              [create delete deletecollection patch update get list watch]
    horizontalpodautoscalers.autoscaling         []                 []              [create delete deletecollection patch update get list watch]
    cronjobs.batch                               []                 []              [create delete deletecollection patch update get list watch]
    jobs.batch                                   []                 []              [create delete deletecollection patch update get list watch]
    daemonsets.extensions                        []                 []              [create delete deletecollection patch update get list watch]
    deployments.extensions/scale                 []                 []              [create delete deletecollection patch update get list watch]
    deployments.extensions                       []                 []              [create delete deletecollection patch update get list watch]
    ingresses.extensions                         []                 []              [create delete deletecollection patch update get list watch]
    networkpolicies.extensions                   []                 []              [create delete deletecollection patch update get list watch]
    replicasets.extensions/scale                 []                 []              [create delete deletecollection patch update get list watch]
    replicasets.extensions                       []                 []              [create delete deletecollection patch update get list watch]
    replicationcontrollers.extensions/scale      []                 []              [create delete deletecollection patch update get list watch]
    ingresses.networking.k8s.io                  []                 []              [create delete deletecollection patch update get list watch]
    networkpolicies.networking.k8s.io            []                 []              [create delete deletecollection patch update get list watch]
    poddisruptionbudgets.policy                  []                 []              [create delete deletecollection patch update get list watch]
    deployments.apps/rollback                    []                 []              [create delete deletecollection patch update]
    deployments.extensions/rollback              []                 []              [create delete deletecollection patch update]
    pods/attach                                  []                 []              [get list watch create delete deletecollection patch update]
    pods/exec                                    []                 []              [get list watch create delete deletecollection patch update]
    pods/portforward                             []                 []              [get list watch create delete deletecollection patch update]
    pods/proxy                                   []                 []              [get list watch create delete deletecollection patch update]
    secrets                                      []                 []              [get list watch create delete deletecollection patch update]
    services/proxy                               []                 []              [get list watch create delete deletecollection patch update]
    bindings                                     []                 []              [get list watch]
    events                                       []                 []              [get list watch]
    limitranges                                  []                 []              [get list watch]
    namespaces/status                            []                 []              [get list watch]
    namespaces                                   []                 []              [get list watch]
    persistentvolumeclaims/status                []                 []              [get list watch]
    pods/log                                     []                 []              [get list watch]
    pods/status                                  []                 []              [get list watch]
    replicationcontrollers/status                []                 []              [get list watch]
    resourcequotas/status                        []                 []              [get list watch]
    resourcequotas                               []                 []              [get list watch]
    services/status                              []                 []              [get list watch]
    controllerrevisions.apps                     []                 []              [get list watch]
    daemonsets.apps/status                       []                 []              [get list watch]
    deployments.apps/status                      []                 []              [get list watch]
    replicasets.apps/status                      []                 []              [get list watch]
    statefulsets.apps/status                     []                 []              [get list watch]
    horizontalpodautoscalers.autoscaling/status  []                 []              [get list watch]
    cronjobs.batch/status                        []                 []              [get list watch]
    jobs.batch/status                            []                 []              [get list watch]
    daemonsets.extensions/status                 []                 []              [get list watch]
    deployments.extensions/status                []                 []              [get list watch]
    ingresses.extensions/status                  []                 []              [get list watch]
    replicasets.extensions/status                []                 []              [get list watch]
    ingresses.networking.k8s.io/status           []                 []              [get list watch]
    poddisruptionbudgets.policy/status           []                 []              [get list watch]
    serviceaccounts                              []                 []              [impersonate create delete deletecollection patch update get list watch]
    ```

    </p>
    </details>
2. 編輯 `files/rbac-edit.yaml` 以修正 namespace
    ```sh

    # Linux:
    $ sed -i 's/<playerid>/'"$USER"'/g' rbac-edit.yaml

    # Mac:
    $ sed -i "" 's/<playerid>/'"$USER"'/g' rbac-edit.yaml
    ```
3. 建立 ClusterRoleBinding
    ```sh
    $ kubectl apply -f rbac-edit.yaml
    ```
3. 重新執行 GitLab CI/CD